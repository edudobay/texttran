defmodule Texttran.Format.Lexer do
  import Unicode.Guards
  import Texttran.Utils.String

  @doc ~S"""
  Hello world.

  ## Examples

      iex> read("")
      []

      iex> read("{}")
      [:begin_block, :end_block]

      iex> read("Record{}")
      [{:identifier, "Record"}, :begin_block, :end_block]

      iex> read("Record \"book\" {}")
      [{:identifier, "Record"}, {:string, "book"}, :begin_block, :end_block]

      iex> read("Record \"\\\"Great\\\" book\" {}")
      [{:identifier, "Record"}, {:string, "\"Great\" book"}, :begin_block, :end_block]

      iex> read("Record{Single}")
      [{:identifier, "Record"}, :begin_block, {:identifier, "Single"}, :end_block]

      iex> read("Record{Line{}}")
      [{:identifier, "Record"}, :begin_block,
        {:identifier, "Line"}, :begin_block, :end_block, :end_block]

      iex> read("Record{} Output json {}")
      [{:identifier, "Record"}, :begin_block, :end_block,
        {:identifier, "Output"}, {:identifier, "json"}, :begin_block, :end_block]

  """
  def read(""), do: []

  def read(format_string) do
    case get_token(format_string) do
      {nil, rest} -> read(rest)
      {token, rest} -> [token | read(rest)]
    end
  end

  @identifier_start_pattern ~r{^[[:alpha:]_]}
  @identifier_end_pattern ~r{(?=[^[:alnum:]_])}

  defp get_token(""), do: {nil, ""}
  defp get_token(<<"{", tail::binary>>), do: {:begin_block, tail}
  defp get_token(<<"}", tail::binary>>), do: {:end_block, tail}

  defp get_token(literal = <<"\"", tail::binary>>) do
    get_string_literal(literal)
  end

  defp get_token(<<space::utf8, tail::binary>>) when is_whitespace(space) do
    tail |> skip_whitespace |> get_token
  end

  defp get_token(string) when is_binary(string) do
    if Regex.match?(@identifier_start_pattern, string) do
      get_identifier(string)
    else
      {:todo, ""}
    end
  end

  defp skip_whitespace(string) do
    {_, rest} = Regex.split(~r{\s*}, string, parts: 2) |> split_pair
    rest || ""
  end

  defp get_identifier(string) do
    {identifier, rest} = Regex.split(@identifier_end_pattern, string, parts: 2) |> split_pair
    {{:identifier, identifier}, rest}
  end

  defp get_string_literal(string) do
    case Texttran.String.Parse.read(string) do
      {literal_value, rest} when is_binary(literal_value) -> {{:string, literal_value}, rest}
    end
  end
end
