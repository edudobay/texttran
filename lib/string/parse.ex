defmodule Texttran.String.Parse do
  import Texttran.Utils.String

  @doc ~S"""

  ## Examples

      iex> read(~S{"literal"})
      {"literal", ""}

      iex> read(~S{"literal" rest})
      {"literal", " rest"}

      iex> read(~S{"this -->\"<-- is a quote"})
      {"this -->\"<-- is a quote", ""}

      iex> read(~S{"tabbed\ttext"})
      {"tabbed\ttext", ""}

  """
  def read(_string = <<"\"", content::binary>>) do
    with {:ok, end_index} <- find_end(content) do
      <<body::binary-size(end_index), "\"", rest::binary>> = content

      {parse_string_content(body), rest}
    end
  end

  @doc ~S"""
  ## Examples

      iex> find_end(~S{"})
      {:ok, 0}

      iex> find_end(~S{\"})
      {:error, :unterminated_string_literal}

      iex> find_end(~S{\""})
      {:ok, 2}

      iex> find_end(~S{a\\\""})
      {:ok, 5}

  """
  def find_end(""), do: {:error, :unterminated_string_literal}

  def find_end("\""), do: {:ok, 0}

  def find_end(string) do
    case find_quote_or_escape(string) do
      nil ->
        {:error, :unterminated_string_literal}

      {_parsed, :string_end, index} ->
        {:ok, index}

      {_parsed, unparsed, index} ->
        with {:ok, new_index} <- find_end(unparsed) do
          {:ok, index + new_index}
        end
    end
  end

  @doc ~S"""
  ## Examples

      iex> find_quote_or_escape(~S{"})
      {"", :string_end, 0}

      iex> find_quote_or_escape(~S{\"})
      {"\"", "", 2}

      iex> find_quote_or_escape(~S{hello})
      {"hello", "", 5}

      iex> find_quote_or_escape(~S{blue"})
      {"blue", :string_end, 4}

  """
  def find_quote_or_escape(string) do
    case Regex.run(~r{\\.|"}, string, return: :index) do
      [{index, _}] ->
        case string do
          <<head::binary-size(index), "\"", _::binary>> ->
            {head, :string_end, index}

          <<head::binary-size(index), "\\", char, rest::binary>> ->
            {head <> <<char>>, rest, index + 2}
        end

      nil ->
        {string, "", byte_size(string)}
    end
  end

  def parse_string_content(string) do
    case Regex.run(~r{\\.}, string, return: :index) do
      nil ->
        string

      [{index, _}] ->
        case string do
          <<prefix::binary-size(index), "\\", char::utf8, rest::binary>> ->
            prefix <> parse_escape_sequence(<<"\\", char>>) <> parse_string_content(rest)
        end
    end
  end

  def parse_escape_sequence(string) do
    case string do
      "\\t" -> "\t"
      "\\n" -> "\n"
      <<"\\", char>> -> <<char>>
    end
  end
end
