defmodule Texttran.Utils.String do
  def split_pair([first]), do: {first, nil}
  def split_pair([first, second]), do: {first, second}
end
