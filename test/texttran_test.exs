defmodule TexttranTest do
  use ExUnit.Case
  doctest Texttran

  defp read_example_item(example_name, item) do
    File.read!("examples/#{example_name}/#{item}")
  end

  defp read_example(example_name),
    do: {
      read_example_item(example_name, "format"),
      read_example_item(example_name, "input"),
      read_example_item(example_name, "output")
    }

  # test "transforms input into output" do
  #  {format, input, output} = read_example("cd-tracks")
  #  assert Texttran.format(format, input) == output
  # end

  @tag :skip
  test "transforms input into output" do
    {format, input, output} = read_example("simple")
    assert Texttran.format(format, input) == output
  end
end
